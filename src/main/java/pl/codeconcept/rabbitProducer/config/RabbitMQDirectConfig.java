package pl.codeconcept.rabbitProducer.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQDirectConfig {

    @Bean
    Queue teksas1Queue() {
        return new Queue("teksas1Queue", false);
    }

    @Bean
    Queue teksas2Queue() {
        return new Queue("teksas2Queue", false);
    }

    @Bean
    Queue teksas3Queue() {
        return new Queue("teksas3Queue", false);
    }

    @Bean
    DirectExchange exchange() {
        return new DirectExchange("direct-exchange");
    }

    @Bean
    Binding teksas1Binding(Queue teksas1Queue, DirectExchange exchange) {
        return BindingBuilder.bind(teksas1Queue).to(exchange).with("teksas1");
    }

    @Bean
    Binding teksas2Binding(Queue teksas2Queue, DirectExchange exchange) {
        return BindingBuilder.bind(teksas2Queue).to(exchange).with("teksas2");
    }

    @Bean
    Binding teksas3Binding(Queue teksas3Queue, DirectExchange exchange) {
        return BindingBuilder.bind(teksas3Queue).to(exchange).with("teksas3");
    }

}
